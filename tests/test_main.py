import yaml

from click.testing import CliRunner
from secretmonger.main import cli


def test_main_base64_encodes_values():
    runner = CliRunner()
    input = {"key1": "value1", "key2": "value2"}
    output = {"key1": "dmFsdWUx", "key2": "dmFsdWUy"}

    with runner.isolated_filesystem():
        with open("input.yml", "w") as file:
            yaml.dump(input, file)

        result = runner.invoke(cli, ["input.yml", "output.yml"])
        assert result.exit_code == 0

        with open("output.yml", "r") as file:
            content = yaml.safe_load(file)

        assert content == output
