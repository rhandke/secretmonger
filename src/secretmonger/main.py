import yaml
import base64

import click


@click.command()
@click.argument("input", type=click.File())
@click.argument("output")
def cli(input, output):
    """Batch base64 encode values

    Read key-value pairs from an INPUT yaml file and create an OUTPUT file
    where the values are base64 encoded.
    """
    content = yaml.safe_load(input)

    with open(output, "w") as file:
        for key in content:
            content[key] = base64.b64encode(content[key].encode()).decode()

        yaml.dump(content, file)
