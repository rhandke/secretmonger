# Secretmonger

Did you ever need to encode a bunch of values to store them in a k8s secret? Ever wondered, if there was a better way than just base64 encoding every single value manually? Then you are in luck. Just create a .yml like this

```
key1: value1
key2: value2
key3: value3
```

and the Secretmonger will create a .yml file with the keys and the encoded values.

# Installation
Clone this project
```
git clone https://gitlab.com/rhandke/secretmonger.git
```

Create and activate virtual environment (optional)
```
cd secretmonger
python3 -m venv .venv
source .venv/bin/activate
```

Run setup
```
pip3 install .
```

# Usage

Run the command and supply your input.yml as well as the name for the output.yml file

```
secret input.yml output.yml
```
